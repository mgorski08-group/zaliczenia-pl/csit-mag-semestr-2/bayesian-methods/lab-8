import arviz as az
import matplotlib.pyplot as plt
import numpy as np
import pymc3 as pm


def generate_dataset(n, p):
    def single_answer():
        if np.random.uniform() < 0.5:
            return np.random.uniform() < p
        else:
            return np.random.uniform() < 0.5

    return [single_answer() for _ in range(n)]


def repeated_survey(times, n, p):
    return [generate_dataset(n, p).count(True) for _ in range(times)]


p = 0.9

rs = repeated_survey(50, 100, 0.1)
fig, ax = plt.subplots(ncols=2)
fig.suptitle("Repeated survey")
fig.set_size_inches(10, 4)
ax[0].hist(rs)
ax[1].plot(rs)
ax[1].set_xlabel("Survey #")
ax[1].set_ylabel("YES answers")
plt.show()

dataset = generate_dataset(100, p)

with pm.Model() as model:
    p_d = pm.Uniform("p_d", lower=0, upper=1)
    p_observed = pm.Deterministic("p_observed", 0.25 + 0.5 * p_d)

    answers = pm.Bernoulli("answers", p_observed, observed=dataset)

    idata = pm.sample(2000, tune=2500)

az.plot_trace(idata)
plt.gcf().suptitle(f"p_d = {p}, n_yes_ans = {dataset.count(True)}")
plt.tight_layout()
plt.show()
